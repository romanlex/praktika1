package com.olin;

public class Main {
    public static int MAX_SESSION_TIME_IN_SECOND = 100;

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < MAX_SESSION_TIME_IN_SECOND; i++) {
                    try {
                        Thread.sleep(1000);
                        Chrono.refresh();
                    } catch (InterruptedException e) {
                        e.getStackTrace();
                    }
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                Chrono.printEachTimeMessage(1, "Session time");
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                Chrono.printEachTimeMessage(5, "msg 1");
            }
        });

        Thread thread4 = new Thread(new Runnable() {
            @Override
            public void run() {
                Chrono.printEachTimeMessage(7, "msg 2");
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }
}
