package com.olin;

/**
 * Created by roman on 09.02.17.
 */
public class Chrono {
    public static final Object lock = new Object();
    public static volatile int chronoTime = 0;

    public static void refresh() {
        chronoTime++;
        synchronized (lock) {
            lock.notifyAll();
        }
    }

    public static void printEachTimeMessage(int eachSecond, String msg) {
        synchronized (lock) {
            try {
                while(true) {
                    if(chronoTime % eachSecond == 0)
                        System.out.println(msg + ": " + Integer.toString(chronoTime));
                    lock.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
